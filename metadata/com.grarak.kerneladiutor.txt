Categories:System
License:Apache2
Web Site:http://forum.xda-developers.com/android/apps-games/app-kernel-adiutor-t2986129
Source Code:https://github.com/Grarak/KernelAdiutor
Issue Tracker:https://github.com/Grarak/KernelAdiutor/issues

Auto Name:Kernel Adiutor
Summary:Manage kernel parameters
Description:
Tweak and monitor kernel parameters. Depending on the device and kernel
used, this includes:

* CPU (Frequency, Governor, Voltages)
* GPU (Frequency, Governor)
* Screen (Color Calibration - RGB)
* Wake (DT2W, S2W)
* Sound (Faux Sound)
* Battery (Fast Charge)
* I/O Scheduler
* Kernel Samepage Merging
* Low Memory Killer (Minfree settings)
* Virtual Machine
* Build prop Editor
* etc.

Features which aren't supported by your device won't show up in the first place.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/Grarak/KernelAdiutor

Build:0.6.2,31
    commit=1f6c14888ba8a551c7321d02d30d194fb03e9368
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.6.3,32
    commit=f5484ba084bc046b5632ce1dbfe21477ba2e292a
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.6.4,33
    commit=47752b4a323298b2f4df9608520b1a97d51b26ac
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.7.1,35
    commit=d353c935b2e684ea0f472634e597be823234baf6
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8,37
    commit=d055d5e6791c3bc11adbd628fb3e64a1bebc9dc0
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.1,38
    commit=d07b366586c6addacb1c98e6683540eeda16f345
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.2,41
    commit=d5696dc6705183fb1360678eace5327d1548dcb1
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.3.1,43
    commit=95966ea175e3efc1912d7acafa6aef46540eafda
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.4,45
    commit=f10fecced32f21af86c6e3bcda93b27e1dba32a0
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.5.1 beta,48
    commit=cbe325c580a3ace9fc6e86d03d1222e17fe60b87
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.5.2 beta,49
    commit=f5230d09248b570f81971e5cb06b819535cc4485
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.6,52
    commit=a3b15f4ac3a465e5490ffa906630d98def47e095
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java src/main/

Build:0.8.6.1 beta,53
    disable=switch to rootshell
    commit=34d8f3d4fd282810d7c006e968ffaea785aa0164
    subdir=app
    gradle=yes
    srclibs=RootShell@1.3
    rm=app/libs/*.jar
    prebuild=cp -fR $$RootShell$$/src/com src/main/

Build:0.9.2 beta,74
    commit=32eaf8f4cb0d5187dec2ff119e364d626fae83a9
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.2.1.1 beta,76
    commit=72b8cb470e7909dd72c3e23f11feb09619686420
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.2.2 beta,77
    commit=1497cd09647ecd05c79ebf1b9969f32cec11ea49
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.3,79
    commit=d438a6a0f8d2262c49e3eb765fd0a0db11d5991d
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.3.1.1 beta,81
    commit=82e8c643b1fb77af5b5376ad49abc83a3e09835b
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.3.1.2 beta,82
    commit=43017a4f7fc4ed5e067108da1985d94cd68cf16b
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.3.1,83
    commit=26a5053574c9b11eed1b327bf223886bf5596dfd
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.3.1.1,84
    commit=4c6041422f04ad6652bf187bc75693f7c12fd0df
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.4 beta,85
    commit=d00673aada67d516e4d4e9cb493f5cdbcc19c2c1
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.4.1 beta,86
    commit=c11423e58bb4258e8af484ca78022338a494d79c
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.4.2,87
    commit=03e41078ba138c3d8698f8c498ed4ab68506681a
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.4.2.1,88
    commit=8c33d52572806bc466b1cb42612a2f388dfd9bf1
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Build:0.9.4.2.2 beta,89
    commit=218de0b6d20567497ec9a6a7223dc8fb7759917b
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.google.android.apps.dashclock:dashclock-api:2.0.0"' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.9.4.2.2 beta
Current Version Code:89

